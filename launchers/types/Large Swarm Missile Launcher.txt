[Configuration]
Name=Large Swarm Missile Launcher
Description=Unleash a swarm of missiles upon your enemies!
Class=Launcher

[Configuration/CompatibleProjectileFamilies]
Medium Missile=true

[LauncherConfiguration/Targeting]
HasTargeting=true

[LauncherConfiguration/FireRate]
ShotsPerMinute=10

[LauncherConfiguration/Burst]
HasBurst=true
ShotsPerBurst=4
TimeBetweenBurstShots=0.05

[LauncherConfiguration/Magazine]
HasMagazine=true
MaxAmmo=20
ReloadTime=60

[LauncherConfiguration/ResourceUsage]
HasResourceUsage=true
ResourcesPerShot.Energy=100
ResourcesPerReload.Energy=5000

[LauncherConfiguration/Capacitor]
HasCapacitor=true
MaxCapacitor=100000
CapacitorFillRate=1000
CapacitorPerShot=1000

[LauncherConfiguration/Fitting]
HasFitting=true
CPU=1000
PG=7000
Slot=Large Weapon

[LauncherConfiguration/Accuracy]
HasAccuracy=true
MinimumSpread=0.01
MaximumSpread=0.015

[LauncherConfiguration/Heat]
HasHeat=true
MaxTemperature=100
ThrottleTemperature=70
ThrottledShotsPerMinute=5
TemperaturePerShot=6
CoolingRate=1

[LauncherConfiguration/ProjectileVelocity]
HasProjectileVelocity=true
RelativeProjectileVelocity=Vector(3500, 0, 0)

[LauncherConfiguration/Sounds]
FireSound=sound/arty/gau.wav