[Configuration]
Name=Mini Mass Accelerator Cannon
Description=SCIENCE!
Class=Launcher

[Configuration/CompatibleProjectileFamilies]
Mini MAC Round=true

[LauncherConfiguration/FireRate]
ShotsPerMinute=60

[LauncherConfiguration/Magazine]
HasMagazine=true
MaxAmmo=1
ReloadTime=60

[LauncherConfiguration/ResourceUsage]
HasResourceUsage=true
ResourcesPerShot.Energy=10
ResourcesPerReload.Energy=500

[LauncherConfiguration/Capacitor]
HasCapacitor=true
FillOnlyWhenFiring=true
MaxCapacitor=3333333
CapacitorFillRate=333333
CapacitorPerShot=3333333
DrainWhenNotFiring=true
CapacitorDrainRate=666666

[LauncherConfiguration/Fitting]
HasFitting=true
CPU=100
PG=100
Slot=Mini Super Weapon

[LauncherConfiguration/Accuracy]
HasAccuracy=true
MinimumSpread=0.0005
MaximumSpread=0.00085

[LauncherConfiguration/Heat]
HasHeat=true
MaxTemperature=100
ThrottleTemperature=70
ThrottledShotsPerMinute=30
TemperaturePerShot=0.5
CoolingRate=10

[LauncherConfiguration/ProjectileVelocity]
HasProjectileVelocity=true
RelativeProjectileVelocity=Vector(16000, 0, 0)

[LauncherConfiguration/Sounds]
StartFiringSound=sound/ship_weapons/wpn_wave_cannon_enabled.wav
FireSound=sound/ship_weapons/mac_gun_fire_2.wav
StopFiringSound=sound/ship_weapons/wpn_wave_cannon_disabled.wav