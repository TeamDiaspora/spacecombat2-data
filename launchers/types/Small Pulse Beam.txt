[Configuration]
Name=Small Pulse Beam
Description=SCIENCE!
Class=Launcher

[Configuration/CompatibleProjectileFamilies]
Small Pulse Laser Crystal=true

[LauncherConfiguration/FireRate]
ShotsPerMinute=100

[LauncherConfiguration/Magazine]
HasMagazine=false

[LauncherConfiguration/ResourceUsage]
HasResourceUsage=true
ResourcesPerShot.Energy=10
ResourcesPerReload.Energy=500

[LauncherConfiguration/Capacitor]
HasCapacitor=true
MaxCapacitor=10000
CapacitorFillRate=800
CapacitorPerShot=150

[LauncherConfiguration/Fitting]
HasFitting=true
CPU=10
PG=100
Slot=Small Weapon

[LauncherConfiguration/Accuracy]
HasAccuracy=false

[LauncherConfiguration/Heat]
HasHeat=true
MaxTemperature=100
ThrottleTemperature=70
ThrottledShotsPerMinute=80
TemperaturePerShot=1.4
CoolingRate=10

[LauncherConfiguration/ProjectileVelocity]
HasProjectileVelocity=false

[LauncherConfiguration/Sounds]
FireSound=sound/spacecombat2/weapons/med_laser_02.wav