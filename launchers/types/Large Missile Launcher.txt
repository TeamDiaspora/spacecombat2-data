[Configuration]
Name=Large Missile Launcher
Description=Unleash a missile upon your enemies!
Class=Launcher

[Configuration/CompatibleProjectileFamilies]
Large Missile=true

[LauncherConfiguration/Targeting]
HasTargeting=true

[LauncherConfiguration/FireRate]
ShotsPerMinute=20

[LauncherConfiguration/Magazine]
HasMagazine=true
MaxAmmo=20
ReloadTime=15

[LauncherConfiguration/ResourceUsage]
HasResourceUsage=true
ResourcesPerShot.Energy=10
ResourcesPerReload.Energy=500

[LauncherConfiguration/Capacitor]
HasCapacitor=true
MaxCapacitor=10000
CapacitorFillRate=200
CapacitorPerShot=1000

[LauncherConfiguration/Fitting]
HasFitting=true
CPU=20
PG=100
Slot=Large Weapon

[LauncherConfiguration/ProjectileVelocity]
HasProjectileVelocity=true
RelativeProjectileVelocity=Vector(3500, 0, 0)

[LauncherConfiguration/Sounds]
FireSound=sound/ship_weapons/wpn_missile.wav