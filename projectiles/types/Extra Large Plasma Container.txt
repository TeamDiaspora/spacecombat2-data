[Configuration]
Name=X-Large Plasma Container
Description=A plasma container that can be fired in many forms.

[Slots/Payload]
X-Large Plasma=true

[Slots/Body]
X-Large Plasma Body=true

[Slots/Hit Effect]
X-Large Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Collision Component