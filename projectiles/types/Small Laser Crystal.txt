[Configuration]
Name=Small Laser Crystal
Description=A small crystal used to change the damage type and color of lasers.

[Slots/Attunement Crystal]
Small Laser Crystal=true

[Slots/Beam Modulator]
Small Beam=true

[MandatoryComponents/Collision Component]
Component=Beam Collision Component

[MandatoryComponents/Movement Component]
Component=Localized Movement Component