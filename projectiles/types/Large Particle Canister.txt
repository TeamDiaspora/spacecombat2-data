[Configuration]
Name=Large Particle Canister
Description=A particle canister that can have various types of particles.

[Slots/Payload]
Large Particle Canister=true

[Slots/Body]
Large Particle Body=true

[Slots/Trail]
Large Trail=true

[Slots/Hit Effect]
Large Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Collision Component