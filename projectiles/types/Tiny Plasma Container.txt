[Configuration]
Name=Tiny Plasma Container
Description=A plasma container that can be fired in many forms.

[Slots/Payload]
Tiny Plasma=true

[Slots/Body]
Tiny Plasma Body=true

[Slots/Hit Effect]
Tiny Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Collision Component