[Configuration]
Name=X-Large Rocket-Boosted Round
Description=An X-Large cannon round that can equip thrusters and either artificial mass or warheads.

[Slots/Payload]
X-Large Warhead=true
X-Large Mass=true

[Slots/Body]
X-Large Projectiles=true

[Slots/Thrusters]
Large Thrusters=true

[Slots/Trail]
X-Large Trail=true

[Slots/Hit Effect]
X-Large Hit Effect=true

[MandatoryComponents/Collision Component]
Component=Collision Component