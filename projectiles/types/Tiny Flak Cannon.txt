[Configuration]
Name=Tiny Flak Cannon
Description=SCIENCE!
Class=Launcher

[Configuration/CompatibleProjectileFamilies]
Tiny Burst Round=true

[LauncherConfiguration/FireRate]
ShotsPerMinute=25

[LauncherConfiguration/Burst]
HasBurst=true
ShotsPerBurst=12
TimeBetweenBurstShots=0.25

[LauncherConfiguration/Magazine]
HasMagazine=true
MaxAmmo=72
ReloadTime=10

[LauncherConfiguration/ResourceUsage]
HasResourceUsage=true
ResourcesPerShot.Energy=10
ResourcesPerReload.Energy=500

[LauncherConfiguration/Capacitor]
HasCapacitor=true
MaxCapacitor=100000
CapacitorFillRate=1000
CapacitorPerShot=100

[LauncherConfiguration/Fitting]
HasFitting=true
CPU=10
PG=100
Slot=Tiny Weapon

[LauncherConfiguration/Accuracy]
HasAccuracy=true
MinimumSpread=0.05
MaximumSpread=0.085

[LauncherConfiguration/Heat]
HasHeat=true
MaxTemperature=100
ThrottleTemperature=70
ThrottledShotsPerMinute=30
TemperaturePerShot=0.5
CoolingRate=10

[LauncherConfiguration/ProjectileVelocity]
HasProjectileVelocity=true
RelativeProjectileVelocity=Vector(18000, 0, 0)