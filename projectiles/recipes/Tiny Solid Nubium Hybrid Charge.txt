[Configuration]
Name=Tiny Solid Nubium Hybrid Charge
OwnerID=NULL
ProjectileType=Tiny Hybrid Charge

[Components/Payload]
Component=Tiny Solid Nubium Mass

[Components/Body]
Component=Tiny Railgun Effect
Configuration.Color=Color(160,255,200)

[Components/Hit Effect]
Component=Tiny Legacy Hit Effect
Configuration.HitEffect=impact_railgun