[Configuration]
Name=X-Large Thermal Missile
OwnerID=NULL
ProjectileType=X-Large Missile

[Components/Payload]
Component=X-Large Low Yield Thermal Warhead

[Components/Body]
Component=X-Large Missile Body

[Components/Thrusters]
Component=X-Large Homing Thruster

[Components/Trail]
Component=X-Large Missile Trail

[Components/Hit Effect]
Component=X-Large Legacy Hit Effect
Configuration.HitEffect=impact_medmissile