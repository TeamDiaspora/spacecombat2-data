[Configuration]
Name=Small Thermal Missile
OwnerID=NULL
ProjectileType=Small Missile

[Components/Payload]
Component=Small Low Yield Thermal Warhead

[Components/Body]
Component=Small Missile Body

[Components/Thrusters]
Component=Small Homing Thruster

[Components/Trail]
Component=Small Missile Trail

[Components/Hit Effect]
Component=Small Legacy Hit Effect
Configuration.HitEffect=impact_medmissile