[Configuration]
Name=Large EM Cannon Round
OwnerID=NULL
ProjectileType=Large Cannon Round

[Components/Trail]
Component=Large Trail
Configuration.Color=Color(255, 205, 0)
Configuration.Length=350
Configuration.Width=60

[Components/Payload]
Component=Large Low Yield EM Warhead

[Components/Body]
Component=Large Projectile Body

[Components/Hit Effect]
Component=Large Legacy Hit Effect
Configuration.HitEffect=impact_railgun
