[Configuration]
Name=Large Antihydrogen Particle Canister
OwnerID=NULL
ProjectileType=Large Particle Canister

[Components/Payload]
Component=Large Antihydrogen Particle Canister

[Components/Body]
Component=Large Particle Blaster Effect
Configuration.Color=Color(180,0,100)

[Components/Trail]
Component=Large Trail
Configuration.Color=Color(180,0,100)
Configuration.Length=400
Configuration.Width=75

[Components/Hit Effect]
Component=Large Legacy Hit Effect
Configuration.HitEffect=impact_railgun