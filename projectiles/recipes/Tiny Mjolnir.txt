[Configuration]
Name=Tiny Mjolnir
OwnerID=NULL
ProjectileType=Tiny Missile

[Components/Payload]
Component=Tiny Low Yield EM Warhead

[Components/Body]
Component=Tiny Mjolnir Effect
Configuration.Color=Color(255, 255, 125)

[Components/Thrusters]
Component=Tiny Homing Thruster

[Components/Trail]
Component=Tiny Trail
Configuration.Color=Color(255, 255, 0)
Configuration.Length=500
Configuration.Width=150

[Components/Hit Effect]
Component=Tiny Mjolnir Hit Effect
Configuration.Color=Color(175, 175, 25)