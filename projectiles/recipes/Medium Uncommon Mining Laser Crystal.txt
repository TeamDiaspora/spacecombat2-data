[Configuration]
Name=Medium Uncommon Mining Laser Crystal
OwnerID=NULL
ProjectileType=Medium Mining Laser Crystal

[Components/Attunement Crystal]
Component=Medium Uncommon Mining Attunement Crystal

[Components/Beam Modulator]
Component=Medium Beam Effect
Configuration.Color=Color(255, 120, 0)

