[Configuration]
Name=X-Large EM Laser Crystal
OwnerID=NULL
ProjectileType=X-Large Laser Crystal

[Components/Attunement Crystal]
Component=X-Large EM Attunement Crystal

[Components/Beam Modulator]
Component=X-Large Beam Effect
Configuration.Color=Color(180, 0, 180)
