[Configuration]
Name=Small Plasma Blaster Effect
Description=A plasma blaster effect visual.
Class=PlasmaBlasterEffectComponent
Family=Small Plasma Body

[ClassData]
Scale=0.45
Color=Color(120, 100, 255)

[ToolOptions/Color]
Type=Color
Default=Color(120, 100, 255)