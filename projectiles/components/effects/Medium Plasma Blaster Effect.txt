[Configuration]
Name=Medium Plasma Blaster Effect
Description=A plasma blaster effect visual.
Class=PlasmaBlasterEffectComponent
Family=Medium Plasma Body

[ClassData]
Scale=0.7
Color=Color(160, 100, 255)

[ToolOptions/Color]
Type=Color
Default=Color(160, 100, 255)