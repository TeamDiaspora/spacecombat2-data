[Configuration]
Name=Tiny Plasma Blaster Effect
Description=A plasma blaster effect visual.
Class=PlasmaBlasterEffectComponent
Family=Tiny Plasma Body

[ClassData]
Scale=0.25
Color=Color(100, 100, 255)

[ToolOptions/Color]
Type=Color
Default=Color(100, 100, 255)