[Configuration]
Name=Large Missile Trail
Description=A Missile trail effect visual.
Class=MissileEffectComponent
Family=Large Trail

[ClassData]
Scale=0.35
Color=Color(255, 255, 255)

[ToolOptions/Color]
Type=Color
Default=Color(255, 255, 255)