[Configuration]
Name=Tiny Drone Body
Description=A tiny drone body.
Class=ModelComponent
Family=Tiny Drone Bodies

[ClassData]
Model=models/slyfo_2/gunball.mdl
Offset=Vector(0,0,0)
AngleOffset=Angle(0,0,0)
Scale=1

[ToolOptions/Offset]
Type=Vector
Mode=Slider
Min=-200
Max=200
Default=Vector(0,0,0)

[ToolOptions/AngleOffset]
Type=Angle
Mode=Slider
Min=-180
Max=180
Default=Angle(0,0,0)

[ToolOptions/Scale]
Type=Number
Mode=Slider
Min=1
Max=2
Default=1

[ToolOptions/Model]
Type=String
Mode=ComboBox
Options.1=models/slyfo_2/gunball.mdl
Options.2=models/food/burger.mdl
Options.3=models/food/hotdog.mdl
Default=models/slyfo_2/gunball.mdl