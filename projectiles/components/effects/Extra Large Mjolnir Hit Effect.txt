[Configuration]
Name=X-Large Mjolnir Hit Effect
Description=A hit effect visual.
Class=HitEffectComponent
Family=X-Large Hit Effect

[ClassData]
Scale=0.5
Color=Color(255, 150, 80)
bUsesProjectile=true
HitEffect=MjolnirHitEffectComponent

[ToolOptions/Color]
Type=Color
Default=Color(255, 150, 80)