[Configuration]
Name=Large Pulse Effect
Description=A pulse effect visual.
Class=PulseCannonEffectComponent
Family=Large Shielded Round

[ClassData]
Scale=0.35
Color=Color(100, 100, 255)

[ToolOptions/Color]
Type=Color
Default=Color(100, 100, 255)