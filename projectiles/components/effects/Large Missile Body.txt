[Configuration]
Name=Large Missile Body
Description=A projectile visual.
Class=ModelComponent
Family=Large Missiles

[ClassData]
Model=models/punisher239/punisher239_missile_heavy.mdl
Offset=Vector(0,0,0)
AngleOffset=Angle(0,0,0)
Scale=1

[ToolOptions/Offset]
Type=Vector
Mode=Slider
Min=-200
Max=200
Default=Vector(0,0,0)

[ToolOptions/AngleOffset]
Type=Angle
Mode=Slider
Min=-180
Max=180
Default=Angle(0,0,0)

[ToolOptions/Scale]
Type=Number
Mode=Slider
Min=1
Max=2
Default=1

[ToolOptions/Model]
Type=String
Mode=ComboBox
Options.1=models/slyfo/torpedo2.mdl
Options.2=models/props_phx/torpedo.mdl
Options.3=models/punisher239/punisher239_missile_heavy.mdl
Default=models/punisher239/punisher239_missile_heavy.mdl