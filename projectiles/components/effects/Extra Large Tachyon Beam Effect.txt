[Configuration]
Name=X-Large Tachyon Beam Effect
Description=Tachyon Beam Effect visual.
Class=TachyonBeamEffectComponent
Family=X-Large Tachyon Beam

[ClassData]
Scale=1.5
Color=Color(100, 20, 20)

[ToolOptions/Color]
Type=Color
Default=Color(100, 20, 20)