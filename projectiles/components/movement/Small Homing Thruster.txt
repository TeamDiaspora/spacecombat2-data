[Configuration]
Name=Small Homing Thruster
Description=A small set of thrusters that will guide the projectile to the target
Class=HomingMovementComponent
Family=Small Homing Thrusters
Mass=1

[ClassData]
Speed=5500
AccelerationTime=0.6
TurnRate=1.45
LateralThrust=true
ClientsidePrediction=true
IgnoreProjectileMass=true