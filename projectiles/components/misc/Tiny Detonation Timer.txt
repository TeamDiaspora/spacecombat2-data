[Configuration]
Name=Tiny Detonation Timer
Description=A timer that blows up the projectile after some time.
Class=TimedEventComponent
Family=Tiny Detonation Timer

[ClassData]
EventName=Detonate
Delay=3
Networked=false

[ToolOptions/Delay]
Type=Number
Mode=Slider
Min=0.3
Max=2
Default=0.8