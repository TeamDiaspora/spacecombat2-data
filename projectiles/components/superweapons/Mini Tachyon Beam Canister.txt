[Configuration]
Name=Mini Tachyon Particle Canister
Description=A tachyon beam focus for focusing tachyon beams.
Class=DamageComponent
Family=Mini Tachyon Particle Canister
Mass=1

[ClassData]
Damage.EM=25000
Damage.THERM=12500