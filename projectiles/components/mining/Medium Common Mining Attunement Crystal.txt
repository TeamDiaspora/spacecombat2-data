[Configuration]
Name=Medium Common Mining Attunement Crystal
Description=A medium attunement crystal that does primarily Thermal damage. It's mostly for mining rocks though.
Class=MiningComponent
Family=Medium Mining Laser Crystal
Mass=1

[ClassData]
Damage.EM=50
Damage.THERM=300
Efficiency=0.7
InAtmosphereEfficiency=0.5
MiningSpeed=120
MiningLevel=1
HasCollectionUpgrade=true