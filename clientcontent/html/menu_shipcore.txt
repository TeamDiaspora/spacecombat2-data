<html>
<meta charset="UTF-8">

<head>
    <link rel="stylesheet" href="css/webui-components-all.min.txt">
    <link rel="stylesheet" href="css/spacecombat2.txt">
    <link rel="stylesheet" href="css/elements/corehealth.txt">
</head>

<body class="fullscreen-body">
    <script id="template_inventoryitem" type="text/x-handlebars-template">
        <!-- {{Resource.Name}}, {{ResourceID}} -->
        <div id="{{Panel.ID}}:{{ResourceID}}:inventory-item" onmousedown="InventoryItemSelected(this.id)" class="sc2-inventory-item text-left color-light">
            <div class="sc2-inventory-tooltip width-full">
                {{#unless Panel.HideTooltips}}
                    <div class="sc2-inventory-tooltip-text">
                        OMG A RESOURCE!
                    </div>
                {{/unless}}
                <div class="text-xl flex-row">
                    {{#if Panel.ShowNumberedList}}
                    <span class="text-left pad-right-sm">{{ResourceID}}.</span>
                    {{/if}}
                    {{#if Resource.Icon}}
                    <img src="{{Resource.Icon}}">
                    {{/if}}
                    <span class="text-left flex-col">{{Resource.Name}}</span>
                    <span class="text-right flex-col">{{Resource.NiceAmount}}</span>
                </div>
                <div class="text-md color-info-light flex-row">
                    {{#if Panel.ShowNumberedList}}
                        {{#if Resource.TotalVolume}}
                        <span class="pad-left-xl text-left flex-col">Volume: {{Resource.TotalVolume}}L</span>
                        {{/if}}
                        {{#if Resource.TotalMass}}
                        <span class="text-right flex-col">Mass: {{Resource.TotalMass}}KG</span>
                        {{/if}}
                    {{else}}
                        {{#if Resource.TotalVolume}}
                        <span class="text-left flex-col">Volume: {{Resource.TotalVolume}}L</span>
                        {{/if}}
                        {{#if Resource.TotalMass}}
                        <span class="text-right flex-col">Mass: {{Resource.TotalMass}}KG</span>
                        {{/if}}
                    {{/if}}
                </div>
            </div>
        </div>
    </script>
    <script id="template_inventorypanel" type="text/x-handlebars-template">
        <div class="sc2-window width-full">
            {{#if Panel.Title}}
                <div class="sc2-window-titlebar text-center">
                    <div class="text-xl text-bold color-light">
                        {{Panel.Title}}
                    </div>
                </div>
            {{/if}}
            <div class="sc2-window-content text-center scrollbox" id="{{Panel.ID}}-scrollbox">
                {{#if Resources}}
                    {{#each Resources as |Resource ResourceID|}}
                        {{> InventoryItem Resource=Resource ResourceID=ResourceID Panel=../Panel}}
                    {{/each}}
                {{else}}
                    <div class="text-lg text-bold color-light">
                        No Resources!
                    </div>
                {{/if}}
            </div>
        </div>
    </script>

    <div class="sc2-window centered width-three-quarters min-width-xl max-width-xxl height-three-quarters">
        <div class="sc2-window-titlebar text-center">
            <div class="text-xl text-bold color-light">Ship Core</div>
        </div>
        <div id="tabs1" class="tabs height-full sc2-window-content flex-col">
            <div class="control-group text-center flex-justify-start flex-row flex-justify-space-between width-one-third flex-center flex-size-1" id="tab-control-main">
                <button type="button" data-target="#tab1" id="StartingPrimaryTab"
                    class="tab-activator color-light transition-short">
                    Ship Status
                </button>
                <button type="button" data-target="#tab2" class="tab-activator color-light transition-short">
                    Ship Hold
                </button>
                <button type="button" data-target="#tab3" class="tab-activator color-light transition-short">
                    Linked Modules
                </button>
            </div>
            <div class="flex-row flex-size-18">
                <div class="tab-item flex-auto" id="tab1">
                    <!-- Ship Status -->
                    <div>
                        <div class="flex-row flex-justify-space-around">
                            <div class="sc2-panel flex-size-13 text-center">
                                <div class="flex-col">
                                    <h1 class="color-info-dark">CORE HEALTH</h1>
                                    <div style="height: 40vh; width: 40vw; float: left;">
                                        <canvas class="CoreHealthChart" id="ShipCoreHealth1"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-size-6">
                                <div class="flex-col flex-fit flex-justify-start">
                                    <div class="sc2-panel flex-col flex-auto">
                                        <h1 class="color-info-dark text-center">CORE INFORMATION</h1>
                                        <p class="color-default text-scalable-xl">
                                            Signature Radius: <span>100</span>
                                            <br/>
                                            Shield Peak Recharge: <span>100</span>
                                            <br/>
                                            Shield Recharge Time: <span>1000</span>
                                            <br/>
                                            Effective Hitpoints: <span>1000000</span>
                                            <br/>
                                            Resource Node Radius: <span>1024</span>
                                            <br/>
                                            Ship Class: <span>Cruiser</span>
                                            <br/>
                                            Ship Sub-Class: <span>Destroyer</span>
                                            <br/>
                                            Generator Class: <span>Cruiser</span>
                                        </p>
                                    </div>
                                    <div class="sc2-panel flex-auto">
                                        <canvas class="CoreResistanceChart" id="ShipCoreResistances1"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-item flex-auto" id="tab2">
                    <!-- Ship Hold -->
                    <div>
                        <div class="tabs flex-col" id="tabsNested1a">
                            <div class="control-group text-center text-scalable-sm width-one-quarter align-center flex-row flex-justify-space-between flex-size-1" id="tab-control-hold">
                                <button type="button" data-target="#tabNested1a" id="StartingHoldTab"
                                    class="tab-activator color-light transition-short">
                                    Cargo
                                </button>
                                <button type="button" data-target="#tabNested2a" class="tab-activator color-light transition-short">
                                    Ammo
                                </button>
                                <button type="button" data-target="#tabNested3a" class="tab-activator color-light transition-short">
                                    Liquid
                                </button>
                                <button type="button" data-target="#tabNested4a" class="tab-activator color-light transition-short">
                                    Gas
                                </button>
                            </div>
                            <div class="flex-size-16">
                                <div class="tab-item" id="tabNested1a">
                                    <div id="cargo-storage" class="sc2-inventory-panel flex-auto"></div>
                                </div>
                                <div class="tab-item" id="tabNested2a">
                                    <div id="ammo-storage" class="sc2-inventory-panel flex-auto"></div>
                                </div>
                                <div class="tab-item" id="tabNested3a">
                                    <div id="gas-storage" class="sc2-inventory-panel flex-auto"></div>
                                </div>
                                <div class="tab-item" id="tabNested4a">
                                    <div id="liquid-storage" class="sc2-inventory-panel flex-auto"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-item flex-auto" id="tab3">
                    <!-- Ship Modules -->

                </div>
            </div>
            <button type="button" class="button-xl button-dark width-full" onmousedown="spacecombat.CloseMenu()">Close</button>
        </div>
    </div>

    <script src="js/chart.txt"></script>
    <script src="js/elements/corehealth.txt"></script>
    <script src="js/elements/coreresistances.txt"></script>
    <script src="js/elements/inventorypanel.txt"></script>

    <script src="js/webui-components.min.txt"></script>
    <script src="js/handlebars.min-v4.7.6.txt"></script>

    <script>
        var WebUIReady = false;
        var ActivePrimaryTab = undefined;
        var ActiveHoldTab = undefined;

        webui.ready(function () {
            WebUIReady = true;

            var PrimaryTabControl = ui("#tab-control-main");
            var HoldTabControl = ui("#tab-control-hold");

            ui("#tabs1").on("ui.tabs.change.before", function (Event) {
                var ActivatedTab = ui(Event.srcElement);
                ActivatedTab.addClass("active-tab");
                ActivatedTab.siblings().removeClass("active-tab");

                var TabParent = "#"+ActivatedTab.parent()[0].id;
                if (HoldTabControl.is(TabParent))
                {
                    ActiveHoldTab = ActivatedTab;
                }
                else if (PrimaryTabControl.is(TabParent))
                {
                    ActivePrimaryTab = ActivatedTab;
                }
            });

            ui("#tabs1").tabControl({
                activeTabId: "#tab1",
                activeTabFocused: false,
                transitionDuration: 200,
                transitionType: "fade"
            });

            ActivePrimaryTab = ui("#StartingPrimaryTab");
            ActivePrimaryTab.trigger("click");

            ActiveHoldTab = ui("#StartingHoldTab");
            ActiveHoldTab.trigger("click");

            SetupInventoryPanels();
            setInterval(function(){RefreshInventoryPanels();}, 1000);
        });

        window.addEventListener("keydown", function (Event) {
            // Do nothing if webui isn't ready yet
            if (!WebUIReady) {
                return;
            }

            // Do nothing if the event was already processed
            if (Event.defaultPrevented) {
                return;
            }

            switch (Event.key) {
                // Process main tabs input
                case "a":
                    ActivePrimaryTab.prevSibling(".tab-activator").trigger("click");
                    break;
                case "d":
                    ActivePrimaryTab.nextSibling(".tab-activator").trigger("click");
                    break;
                case "q":
                    ActiveHoldTab.prevSibling(".tab-activator").trigger("click");
                    break;
                case "e":
                    ActiveHoldTab.nextSibling(".tab-activator").trigger("click");
                    break;
                case "ArrowLeft":
                    break;
                case "ArrowRight":
                    break;
                default:
                    return;
            }

            // Cancel the default action to avoid it being handled twice
            Event.preventDefault();
        }, true);
    </script>
</body>

</html>