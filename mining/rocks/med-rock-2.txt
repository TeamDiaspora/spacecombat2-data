[Medium Flat Rock]
Model=models/props_foliage/rock_coast02f.mdl
PossibleElements=Veldspar:500:1,Uranium:50:0.5,Carbon:150:1,Ice:150:0.25,Polonium:25:0.1
MinOre=800000
MaxOre=1250000
Chance=100
SkinType=0
Color=Color(255,255,255)
