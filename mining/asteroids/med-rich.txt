[Medium Rich Asteroid]
Model=models/mandrac/asteroid/rock3.mdl
PossibleElements=Iron:200:0.25,Ice:100:0.5,Trinium:15:0.1,Titanite:15:0.1,Aluminum:15:0.35,Veldspar:10:0.1,Gold:5:0.15
MinOre=45000
MaxOre=75000
Chance=600
SkinType=0
Color=Color(255,255,255)
