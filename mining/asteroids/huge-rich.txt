[Massive Rich Asteroid]
Model=models/ce_ls3additional/asteroids/asteroid_450.mdl
PossibleElements=Ice:75:0.75,Trinium:10:0.5,Titanite:25:0.5,Iron:100:0.25,Veldspar:75:0.1,Depleted Uranium:20:1,Uranium:5:1,Triidium:15:0.1,Tronium:5:0.1
MinOre=190000
MaxOre=280000
Chance=125
SkinType=0
Color=Color(255,255,255)
