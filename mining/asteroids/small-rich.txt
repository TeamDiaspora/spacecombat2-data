[Small Rich Asteroid]
Model=models/mandrac/asteroid/pyroxveld3.mdl
PossibleElements=Iron:200:0.25,Ice:100:0.5,Veldspar:10:0.1,Aluminum:15:0.35,Gold:5:0.15
MinOre=20000
MaxOre=50000
Chance=650
SkinType=1
Color=Color(101,106,117)
