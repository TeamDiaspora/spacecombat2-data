[Asteroid Belt]
Type=Belt
Pos1=2800,4000,9300
Pos2=15400,1900,9600
MaxCount=30
Variance=1500
Ignore Atmospheres=0

[Ressk Mineral Cave]
Type=Crystal Field
Pos=8283,-8682,-6590
MaxCount=150
ScanHeight=75
RespawnRate=240
MinInitialSpawn=75
FieldHealth=500000
CrystalType=2
Ignore Atmospheres=0