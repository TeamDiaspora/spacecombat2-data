[Black Hole Ring]
Type=Ring
Pos=9476,8181,9288
Radius=7200
InnerRadius=4450
Inclination=21.191
Azimuth=90
Thickness=100
MaxCount=100
Ignore Atmospheres=0

[Center Asteroid Cloud]
Type=Simple Cloud
Pos=6063,417,2323
MaxCount=20
XRandom=3000
YRandom=3000
ZRandom=2500
Ignore Atmospheres=0

[Corner Asteroid Cloud]
Type=Simple Cloud
Pos=-10888,-9210,-4234
MaxCount=65
XRandom=8000
YRandom=8000
ZRandom=5000
Ignore Atmospheres=0

[Mineral Field]
Type=Crystal Field
Pos=6500,4430,6152
MaxCount=70
MaxDistance=3000
ScanHeight=500
RespawnRate=240
MinInitialSpawn=50
FieldHealth=500000
CrystalMaterialColor=Vector(2, 0.4, 0)
CrystalEnvColor=Vector(0.65, 0.12, 0.12)
CrystalParticleColor=Color(255, 0, 0, 65)
Ignore Atmospheres=0

[Ice Mineral Field]
Type=Crystal Field
Pos=-1020,-825,-3236
MaxCount=50
MaxDistance=1750
ScanHeight=500
RespawnRate=240
MinInitialSpawn=30
FieldHealth=500000
CrystalMaterialColor=Vector(0.6, 0.75, 0.73)
CrystalEnvColor=Vector(0.55, 0.55, 0.7)
CrystalParticleColor=Color(0, 40, 255, 65)
Ignore Atmospheres=0
Ignore Displacements=1