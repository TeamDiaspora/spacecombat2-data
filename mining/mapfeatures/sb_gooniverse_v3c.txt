[Asteroid Belt]
Type=Belt
Pos1=-15200,12400,12100
Pos2=-7100,-5100,-600
MaxCount=50
Variance=1500
Ignore Atmospheres=0

[Gooniverse Ring]
Type=Ring
Pos=-8192,8704,10240
Radius=7200
InnerRadius=4450
Inclination=17.191
Azimuth=353.882
Thickness=100
MaxCount=100
Ignore Atmospheres=0

[Corner Asteroid Cloud]
Type=Simple Cloud
Pos=13565,-8538,12011
MaxCount=65
XRandom=8000
YRandom=8000
ZRandom=5000
Ignore Atmospheres=0

[Center Asteroid Cloud]
Type=Simple Cloud
Pos=6632,-1294,3269
MaxCount=20
XRandom=3000
YRandom=3000
ZRandom=4500
Ignore Atmospheres=0

[EndGame Ring]
Type=Ring
Pos=1510,7720,-10296
Radius=6200
InnerRadius=4450
Inclination=44
Azimuth=245
Thickness=500
MaxCount=20
Ignore Atmospheres=0

[Cerebus Mineral Field]
Type=Crystal Field
Pos=6884,-12152,-2048
MaxCount=100
MaxDistance=3000
ScanHeight=150
RespawnRate=240
MinInitialSpawn=100
FieldHealth=500000
CrystalMaterialColor=Vector(6, 2, 0)
CrystalEnvColor=Vector(0.2, 0.3, 0.4)
CrystalParticleColor=Color(255, 255, 100, 120)
Ignore Atmospheres=0

[EndGame Mineral Field]
Type=Crystal Field
Pos=1260, 7541, -11249
MaxCount=60
MaxDistance=3000
ScanHeight=150
RespawnRate=240
MinInitialSpawn=50
FieldHealth=500000
CrystalMaterialColor=Vector(5, 0.8, 0)
CrystalEnvColor=Vector(0.6, 0.12, 0.12)
CrystalParticleColor=Color(255, 0, 0, 65)
Ignore Atmospheres=0