[Environment/Hiigara]
Location=-9728.000000 -6144.000000 -8192.000000
Type=planet
Gravity=1
Temperature=288
Atmosphere=1
Radius=4864

[EnvironmentResources/Hiigara]
Carbon Dioxide=3405504
Hydrogen=4162283
Oxygen=158923549
Nitrogen=454067283
Methane=26487258
Carbon Monoxide=34055046
Water=208114171


[Environment/Cerberus]
Location=8192.000000 -10240.000000 -2040.000000
Type=planet
Gravity=1.5
Temperature=650
Atmosphere=3
Radius=4832

[EnvironmentResources/Cerberus]
Carbon Dioxide=678875495
Oxygen=741940
Nitrogen=25967915
Methane=25967915
Carbon Monoxide=33387319
Water=111291064


[Environment/Kobol]
Location=9726.900391 9216.809570 4360.000000
Type=planet
Gravity=1
Temperature=288
Atmosphere=1
Radius=4032

[EnvironmentResources/Kobol]
Carbon Dioxide=1939824
Hydrogen=2370896
Oxygen=90525123
Nitrogen=258643210
Methane=15087520
Carbon Monoxide=19398240
Water=118544804


[Environment/Demeter]
Location=-8192.000000 8704.000000 10240.000000
Type=planet
Gravity=0.5
Temperature=60
Atmosphere=1
Radius=3328

[EnvironmentResources/Demeter]
Carbon Dioxide=221798859
Oxygen=242403
Nitrogen=8484109
Methane=8484109
Carbon Monoxide=10908140
Water=36360468


[Environment/End_Game]
Location=1536.000000 7679.999512 -10240.000000
Type=planet
Gravity=0.6
Temperature=450
Atmosphere=1
Radius=3008

[EnvironmentResources/End_Game]
Carbon Dioxide=163773206
Oxygen=178987
Nitrogen=6264548
Methane=6264548
Carbon Monoxide=8054419
Water=26848066


[Environment/Coruscant]
Location=2.338870 -1.374510 4620.000000
Type=planet
Gravity=1
Temperature=400
Atmosphere=1
Radius=1120

[EnvironmentResources/Coruscant]
Carbon Dioxide=41577
Hydrogen=50816
Oxygen=1940267
Nitrogen=5543621
Methane=323377
Carbon Monoxide=415771
Water=2540826
