[Environment/Ochl]
Location=-8996.000000 8636.000000 -6499.999512
Type=planet
Gravity=1
Temperature=300
Atmosphere=1
Radius=4188

[EnvironmentResources/Ochl]
Carbon Dioxide=20409623
Hydrogen=61228870
Oxygen=285734729
Nitrogen=40819247
Methane=16907380
Carbon Monoxide=21738060
Water=132843703


[Environment/Umemeru]
Location=9157.000000 9641.000000 7966.000000
Type=planet
Gravity=1
Temperature=350
Atmosphere=1
Radius=4096

[EnvironmentResources/Umemeru]
Carbon Dioxide=267314707
Hydrogen=57281723
Oxygen=19093907
Nitrogen=38187815
Methane=15817438
Carbon Monoxide=20336706
Water=124279872


[Environment/Nyem]
Location=9280.000000 -9728.000000 -9848.570312
Type=planet
Gravity=1
Temperature=215
Atmosphere=1
Radius=4096

[EnvironmentResources/Nyem]
Carbon Dioxide=267314707
Hydrogen=19093907
Oxygen=76375630
Nitrogen=19093907
Methane=15817438
Carbon Monoxide=20336706
Water=124279872


[Environment/Unedari]
Location=10088.000000 -10560.000000 11088.000000
Type=planet
Gravity=1
Temperature=350
Atmosphere=1
Radius=3072

[EnvironmentResources/Unedari]
Carbon Dioxide=8055242
Hydrogen=24165726
Oxygen=112773392
Nitrogen=16110484
Methane=6672981
Carbon Monoxide=8579548
Water=52430571


[Environment/Nuyitae]
Location=-928.000000 -1919.999878 2336.000000
Type=planet
Gravity=1
Temperature=300
Atmosphere=1
Radius=3072

[EnvironmentResources/Nuyitae]
Carbon Dioxide=8055242
Hydrogen=24165726
Oxygen=112773392
Nitrogen=16110484
Methane=6672981
Carbon Monoxide=8579548
Water=52430571


[Environment/Inyat]
Location=-9792.000000 -10432.000000 9663.000000
Type=planet
Gravity=1
Temperature=700
Atmosphere=1
Radius=3072

[EnvironmentResources/Inyat]
Carbon Dioxide=32220969
Hydrogen=112773392
Oxygen=8055242
Nitrogen=8055242
Methane=6672981
Carbon Monoxide=8579548
Water=52430571


[Environment/The asteroid]
Location=5846.000000 6553.000000 -8713.000000
Type=planet
Gravity=1
Temperature=14
Atmosphere=1
Radius=2048

[EnvironmentResources/The asteroid]
Methane=1977179
Carbon Monoxide=2542088
Water=15534984


[Environment/Umemeru's Oil Drilling Amtosphere]
Location=8576.000000 9408.000000 8000.000000
Type=planet
Gravity=1
Temperature=350
Atmosphere=1
Radius=512

[EnvironmentResources/Umemeru's Oil Drilling Amtosphere]
Carbon Dioxide=37292
Hydrogen=111878
Oxygen=522099
Nitrogen=74585
Methane=30893
Carbon Monoxide=39720
Water=242734


[Environment/The Easter Egg...  Have fun]
Location=6158.000000 5640.000000 -10864.000000
Type=planet
Gravity=1
Temperature=300
Atmosphere=1
Radius=512

[EnvironmentResources/The Easter Egg...  Have fun]
Carbon Dioxide=37292
Hydrogen=111878
Oxygen=522099
Nitrogen=74585
Methane=30893
Carbon Monoxide=39720
Water=242734


[Environment/Inyat's Geothermal Atmosphere]
Location=-9008.000000 -10655.999023 9695.250000
Type=planet
Gravity=1
Temperature=350
Atmosphere=1
Radius=512

[EnvironmentResources/Inyat's Geothermal Atmosphere]
Carbon Dioxide=37292
Hydrogen=111878
Oxygen=522099
Nitrogen=74585
Methane=30893
Carbon Monoxide=39720
Water=242734
