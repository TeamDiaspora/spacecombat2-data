[Environment/desert]
Location=9216.000000 -9216.000000 2048.000000
Type=planet
Gravity=1.25
Temperature=373
Atmosphere=1
Radius=4608

[EnvironmentResources/desert]
Carbon Dioxide=54372885
Hydrogen=43498308
Oxygen=10874577
Nitrogen=434983084
Methane=22521313
Carbon Monoxide=28955974
Water=176953178


[Environment/earthlike]
Location=0.000000 9216.000000 -3072.000000
Type=planet
Gravity=1
Temperature=293
Atmosphere=1
Radius=4096

[EnvironmentResources/earthlike]
Carbon Dioxide=7637563
Hydrogen=30550252
Oxygen=152751261
Nitrogen=190939077
Methane=15817438
Carbon Monoxide=20336706
Water=124279872


[Environment/420BlazeIt]
Location=-9216.000000 -9216.000000 -512.000000
Type=planet
Gravity=1
Temperature=323
Atmosphere=1
Radius=4096

[EnvironmentResources/420BlazeIt]
Carbon Dioxide=95469538
Hydrogen=152751261
Oxygen=19093907
Nitrogen=114563446
Methane=15817438
Carbon Monoxide=20336706
Water=124279872


[Environment/mineral]
Location=0.000000 -4096.000000 -8192.000000
Type=planet
Gravity=1
Temperature=223
Atmosphere=1
Radius=3072

[EnvironmentResources/mineral]
Hydrogen=64441938
Nitrogen=96662907
Methane=6672981
Carbon Monoxide=8579548
Water=52430571


[Environment/lava]
Location=9216.000000 3072.000000 10240.000000
Type=planet
Gravity=0.8
Temperature=693
Atmosphere=1
Radius=3072

[EnvironmentResources/lava]
Carbon Dioxide=112773392
Hydrogen=16110484
Nitrogen=32220969
Methane=6672981
Carbon Monoxide=8579548
Water=52430571


[Environment/ice]
Location=-9216.000000 2048.000000 -11264.000000
Type=planet
Gravity=0.6
Temperature=73
Atmosphere=1
Radius=3072

[EnvironmentResources/ice]
Hydrogen=112773392
Nitrogen=48331453
Methane=6672981
Carbon Monoxide=8579548
Water=52430571


[Environment/moon]
Location=-8192.000000 11264.000000 1024.000000
Type=planet
Gravity=0.4
Temperature=191
Atmosphere=0
Radius=1536

[EnvironmentResources/moon]
Methane=834122
Carbon Monoxide=1072443
Water=6553821
